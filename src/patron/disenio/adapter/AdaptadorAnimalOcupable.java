package patron.disenio.adapter;

public class AdaptadorAnimalOcupable implements Ocupable {
	
	private Animal animal;

	public AdaptadorAnimalOcupable(Animal animal) {
		this.animal = animal;
	}

	public Animal getAnimal() {
		return animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	@Override
	public void realizarOcupacion() {
		animal.ejecutarAccionesAnimal();
	}

}
