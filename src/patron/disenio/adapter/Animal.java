package patron.disenio.adapter;

public class Animal {
	
	private String tipo;
	
	private String sonido;

	public Animal(String tipo, String sonido) {
		this.tipo = tipo;
		this.sonido = sonido;
	}
	
	public void ejecutarAccionesAnimal(){
		System.out.println("El animal es: " + tipo + ", y el sonido que hace es: " + sonido);
	}

}
