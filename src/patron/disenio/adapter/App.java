package patron.disenio.adapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class App {

	public static void main(String[] args) {

		// Creo los objetos ocupables
		List<Ocupable> elementosOcupables = new ArrayList<Ocupable>();
		elementosOcupables.add(new Empleado("Juan", "Perez", 28, "Telecentro", 3, "Contador"));
		elementosOcupables.add(new Estudiante("Javier", "Gomez", 11, 6, 'A'
				, Arrays.asList("Literatura", "Matematica", "Ciencias Sociales", "Ciencias Naturales")));
		elementosOcupables.add(new AdaptadorAnimalOcupable(new Animal("Perro", "ladrar")));
		elementosOcupables.add(new AdaptadorAnimalOcupable(new Animal("Gato", "maullar")));
	
		mostrarInformacionYOcupaciones(elementosOcupables);
	}

	private static void mostrarInformacionYOcupaciones(List<Ocupable> elementosOcupables) {

		Iterator<Ocupable> ocupablesIterator = elementosOcupables.iterator();
		
		while(ocupablesIterator.hasNext()) {
			ocupablesIterator.next().realizarOcupacion();
		}
		
	}

}
