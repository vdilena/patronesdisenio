package patron.disenio.adapter;

public interface Ocupable {
	
	void realizarOcupacion();

}
