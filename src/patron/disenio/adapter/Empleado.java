package patron.disenio.adapter;

public class Empleado extends Persona implements Ocupable {
	
	String nombreTrabajo;
	
	int antiguedad;
	
	String puesto;

	public Empleado(String nombre, String apellido, int edad, String nombreTrabajo, int antiguedad, String puesto) {

		super(nombre, apellido, edad);
		this.nombreTrabajo = nombreTrabajo;
		this.antiguedad = antiguedad;
		this.puesto = puesto;
	}

	@Override
	public void realizarOcupacion() {
		
		mostrarDatos();
		System.out.println("Nombre trabajo: " + nombreTrabajo + ", antiguedad: " + antiguedad + ", Puesto: " + puesto);
	}

}
