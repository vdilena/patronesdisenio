package patron.disenio.adapter;

import java.util.Iterator;
import java.util.List;

public class Estudiante extends Persona implements Ocupable {
	
	private int grado;
	
	private char division;
	
	private List<String> materias;

	public Estudiante(String nombre, String apellido, int edad, int grado, char division
			, List<String> materias) {
		
		super(nombre, apellido, edad);
		this.grado = grado;
		this.division = division;
		this.materias = materias;
	}

	@Override
	public void realizarOcupacion() {
		
		mostrarDatos();
		System.out.println("Grado: " + grado + ", division: " + division);
		System.out.println("Materias:");
		
		Iterator<String> iteradorMaterias = materias.iterator();
		while(iteradorMaterias.hasNext()) {
			System.out.println(". " + iteradorMaterias.next());
		}
	}

}
