package patron.disenio.strategy;

public class AnimalVoladorStrategy implements AccionAnimalStrategy {

	@Override
	public void ejecutarAccion() {
		volar();
	}
	
	private void volar(){
		System.out.println("Estoy volando");
	}

}
