package patron.disenio.strategy;

public class Canino extends Animal {
	
	private int cantidadDientes;
	private int cantidadPatas;
	
	public Canino(String nombre, int cantidadDientes, int cantidadPatas, AccionAnimalStrategy estrategiaCanino) {
		
		super(nombre, estrategiaCanino);
		this.cantidadPatas = cantidadPatas;
		this.cantidadDientes = cantidadDientes;
	}

	public int getCantidadDientes() {
		return cantidadDientes;
	}

	@Override
	public void displayPropiedades() {
		
		System.out.println("Soy un " + this.getNombre() + ", tengo " + cantidadDientes 
				+ " dientes y " + cantidadPatas + " patas y ahora ");
	}

}
