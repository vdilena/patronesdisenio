package patron.disenio.strategy;

public class Pez extends Animal {
	
	private int cantidadAletas;
	
	public Pez(String nombre, int cantidadAletas, AccionAnimalStrategy estrategiaPez) {
		
		super(nombre, estrategiaPez);
		this.cantidadAletas = cantidadAletas;
	}

	public int getCantidadAletas() {
		return cantidadAletas;
	}

	@Override
	public void displayPropiedades() {
		
		System.out.println("Soy un " + this.getNombre() + ", tengo " + cantidadAletas 
				+ " aletas y ahora ");
	}

}
