package patron.disenio.strategy;

public interface AccionAnimalStrategy {
	
	void ejecutarAccion();

}
