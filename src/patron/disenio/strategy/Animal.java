package patron.disenio.strategy;

public abstract class Animal {
	
	private AccionAnimalStrategy accion;
	private String nombre;

	
	public Animal(String nombre, AccionAnimalStrategy estrategia) {

		this.nombre = nombre;
		this.accion = estrategia;
	}
	
	public AccionAnimalStrategy getAccion() {
		return accion;
	}

	public String getNombre() {
		return nombre;
	}

	public void ejecutarAccionMotriz() {
		
		this.displayPropiedades();
		this.accion.ejecutarAccion();
	};
	
	public abstract void displayPropiedades();
}
