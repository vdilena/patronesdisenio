package patron.disenio.strategy;

public class Ave extends Animal {
	
	private int cantidadAlas;

	public Ave(String nombre, int cantidadAlas, AccionAnimalStrategy estrategiaVolador){
	
		super(nombre, estrategiaVolador);
		this.cantidadAlas = cantidadAlas;
	}
	
	public int getCantidadAlas() {
		return cantidadAlas;
	}

	@Override
	public void displayPropiedades() {
		
		System.out.println("Soy un " + this.getNombre() + ", tengo " + cantidadAlas 
				+ " alas y ahora ");
	}

}
