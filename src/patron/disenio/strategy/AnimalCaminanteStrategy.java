package patron.disenio.strategy;

public class AnimalCaminanteStrategy implements AccionAnimalStrategy {

	@Override
	public void ejecutarAccion() {
		caminar();
	}
	
	private void caminar() {
		System.out.println("Estoy caminando");
	}

}
