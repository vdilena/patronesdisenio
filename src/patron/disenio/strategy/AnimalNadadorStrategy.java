package patron.disenio.strategy;

public class AnimalNadadorStrategy implements AccionAnimalStrategy {

	@Override
	public void ejecutarAccion() {
		nadar();
	}
	
	private void nadar(){
		System.out.println("Estoy nadando");
	}

}
