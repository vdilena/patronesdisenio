package patron.disenio.strategy;

import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) {
		
		List<Animal> animales = new ArrayList<Animal>();

		Canino perro = new Canino("Perro", 30, 4, new AnimalCaminanteStrategy());
		animales.add(perro);
		
		Ave hornero = new Ave("Hornero", 2, new AnimalVoladorStrategy());
		animales.add(hornero);
		
		Pez corvina = new Pez("Corvina", 2, new AnimalNadadorStrategy());
		animales.add(corvina);
		
		Ave avestruz = new Ave("Avestruz", 2, new AnimalCaminanteStrategy());
		animales.add(avestruz);
		
		Pez pezRana = new Pez("Pez Rana", 2, new AnimalCaminanteStrategy());
		animales.add(pezRana);
		
		animales.forEach(animal -> animal.ejecutarAccionMotriz());
		
	}

}
