package patron.disenio.observer;

public class App {

	public static void main(String[] args) {


		AutorLibro unAutor = new AutorLibro("George Orwell");
		Libreria libreriaPalermo = new Libreria(unAutor);
		Libreria libreriaBelgrano = new Libreria(unAutor);
		Escuela escuelaMoron = new Escuela(unAutor);
		Escuela escuelaCastelar = new Escuela(unAutor);
		
		// El autor publica un nuevo libro
		unAutor.agregarNuevoLibro(new Libro("1984", 15465465, 1949, unAutor));
		unAutor.agregarNuevoLibro(new Libro("Rebelion en la granja", 954454, 1945, unAutor));
		
		// Muestro libros disponibles de cada instituto
		libreriaPalermo.mostrarInformacionDeLibros();
		libreriaBelgrano.mostrarInformacionDeLibros();
		escuelaMoron.mostrarInformacionDeLibros();
		escuelaCastelar.mostrarInformacionDeLibros();
		
	}

}
