package patron.disenio.observer;

public interface Observer {

	public void update(Object elementoActualizador);
}
