package patron.disenio.observer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Escuela implements Observer {

	private Observable observable;
	private List<Libro> libros;
	
	public Escuela(Observable observable){
		
		this.setObservable(observable);
		observable.addObserver(this);
		this.libros = new ArrayList<Libro>();
	}
	
	@Override
	public String toString() {

		String textoAMmostrar = "Estos son los libros que tiene (o esta proximo a tener) la excuela: \n";
		
		Iterator<Libro> iteradorLibros = libros.iterator();
		while (iteradorLibros.hasNext()) {
			
			Libro libroActual = iteradorLibros.next();
			textoAMmostrar = textoAMmostrar.concat("Libro: " 
					+ libroActual.getNombre() + ", del autor: " + libroActual.getAutor().getNombre() + "\n");
		}
		
		return textoAMmostrar;
	}
	
	public void mostrarInformacionDeLibros() {
		System.out.println(this.toString());
	}
	
	@Override
	public void update(Object nuevoLibro) {
		libros.add((Libro) nuevoLibro);
	}

	public Observable getObservable() {
		return observable;
	}

	public void setObservable(Observable observable) {
		this.observable = observable;
	}
}
