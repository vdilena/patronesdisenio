package patron.disenio.observer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Libreria implements Observer {
	
	private Observable observable;
	private List<Libro> libros;
	
	public Libreria(Observable observable) {
		
		this.setObservable(observable);
		observable.addObserver(this);
		this.libros = new ArrayList<Libro>();
	}
	
	@Override
	public String toString() {

		String textoAMmostrar = "Estos son los libros que tiene (o esta proximo a tener) la libreria: \n";
		
		Iterator<Libro> iteradorLibros = libros.iterator();
		while (iteradorLibros.hasNext()) {
			
			Libro libroActual = iteradorLibros.next();
			textoAMmostrar = textoAMmostrar.concat("Libro: " 
					+ libroActual.getNombre() + ", del autor: " + libroActual.getAutor().getNombre() + "\n");
		}
		
		return textoAMmostrar;
	}
	
	public void mostrarInformacionDeLibros() {
		System.out.println(this.toString());
	}

	@Override
	public void update(Object elementoActualizador) {
		this.libros.add((Libro) elementoActualizador);
	}

	public Observable getObservable() {
		return observable;
	}

	public void setObservable(Observable observable) {
		this.observable = observable;
	}

}
