package patron.disenio.observer;

public class Libro {

	private String nombre;
	
	private Integer isbn;
	
	private Integer anioPublicacion;
	
	private AutorLibro autor;

	public Libro(String nombre, Integer isbn, Integer anioPublicacion, AutorLibro autor) {

		this.nombre = nombre;
		this.isbn = isbn;
		this.anioPublicacion = anioPublicacion;
		this.autor = autor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getIsbn() {
		return isbn;
	}

	public void setIsbn(Integer isbn) {
		this.isbn = isbn;
	}

	public Integer getAnioPublicacion() {
		return anioPublicacion;
	}

	public AutorLibro getAutor() {
		return autor;
	}

	public void setAutor(AutorLibro autor) {
		this.autor = autor;
	}

	public void setAnioPublicacion(Integer anioPublicacion) {
		this.anioPublicacion = anioPublicacion;
	}
}
