package patron.disenio.observer;

import java.util.ArrayList;
import java.util.List;

public class AutorLibro implements Observable {
	
	private List<Observer> listaObservadores;
	private String nombre;
	private List<Libro> libros;
	
	public AutorLibro(String nombre) {
		
		this.listaObservadores = new ArrayList<Observer>();
		this.nombre = nombre;
		this.libros = new ArrayList<Libro>();
	}
	
	public void publicarLibro(Libro nuevoLibro) {
		
		// Notifico a todas las librerias, sobre el nuevo libro, para que lo agreguen a sus colecciones
		this.notifyObservers(nuevoLibro);
	}
	
	public void agregarNuevoLibro(Libro nuevoLibro) {
		
		this.libros.add(nuevoLibro);
		this.publicarLibro(nuevoLibro);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public void addObserver(Observer ob) {
		listaObservadores.add(ob);
	}

	@Override
	public void removeObserver(Observer ob) {
		listaObservadores.remove(ob);
	}

	@Override
	public void notifyObservers(Object nuevoLibro) {
		
		for(Observer observador : listaObservadores) {
			observador.update(nuevoLibro);
		}
	}

}
