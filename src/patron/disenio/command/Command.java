package patron.disenio.command;

public interface Command {

	void execute();
}
