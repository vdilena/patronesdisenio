package patron.disenio.command;

import java.util.ArrayList;
import java.util.List;

public class VendedorInvoker {

	private List<Command> listaOrdenes = new ArrayList<Command>();

	// La orden puede ser de compra o venta
	public void tomarOrden(Command orden) {
		listaOrdenes.add(orden);
	}

	
	public void ejecutarOrdenes() {

		for (Command orden : listaOrdenes) {
			orden.execute();
		}
		listaOrdenes.clear();
	}
}
