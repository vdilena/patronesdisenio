package patron.disenio.command;

public class ComprarProductoCommand implements Command {

	private Producto producto;

	public ComprarProductoCommand(Producto producto) {
		this.producto = producto;
	}

	public void execute() {
		producto.comprar();
	}

}
