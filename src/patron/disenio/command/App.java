package patron.disenio.command;

public class App {

	public static void main(String[] args) {

		// El cliente hace el pedido
		Producto televidorLG = new Producto("Telvisor LG", 2, 50000.0);

		// Se generan los comandos para hacer las ordenes de compra y venta
		ComprarProductoCommand ordenCompraTelevisorLG = new ComprarProductoCommand(televidorLG);
		VenderProductoCommand ordenVentaTelevisorLG = new VenderProductoCommand(televidorLG);

		// El vendedor, toma la orden de compra y la orden de venta
		VendedorInvoker vendedor = new VendedorInvoker();
		vendedor.tomarOrden(ordenCompraTelevisorLG);
		vendedor.tomarOrden(ordenVentaTelevisorLG);

		// El vendedor ejecuta la lista de ordenes
		vendedor.ejecutarOrdenes();
	}
}
