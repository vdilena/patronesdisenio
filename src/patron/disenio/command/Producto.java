package patron.disenio.command;

public class Producto {

	private String nombre;
	private int cantidad;
	private Double precio;

	public Producto(String nombre, int cantidad, Double precio) {

		this.nombre = nombre;
		this.cantidad = cantidad;
		this.precio = precio;
	}

	public void comprar() {
		System.out.println(
				"Producto comprado -> Nombre: " + nombre + ", Cantidad: " + cantidad + " , Precio: $: " + precio);
	}

	public void vender() {
		System.out.println(
				"Producto vendido -> Nombre: " + nombre + ", Cantidad: " + cantidad + " , Precio: $: " + precio);
	}
}
