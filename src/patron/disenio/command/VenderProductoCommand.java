package patron.disenio.command;

public class VenderProductoCommand implements Command {

	private Producto producto;

	public VenderProductoCommand(Producto producto) {
		this.producto = producto;
	}

	public void execute() {
		producto.vender();
	}

}
